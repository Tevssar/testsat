<?php
/**
 * Class for maximum string lenght validation
 *
 * @category Class
 * @package  Satomrutest
 * @author   Bogdan <tevssar379@gmail.com>
 */
namespace Satomrutest\FormValidtor;

/**
 * Class for maximum string lenght validation
 * 
 * @method rule()
 */
class MaxLenghtValidator extends Validator
{
    /**
     * Maximum string lenght validation rule, return false if validation fail.
     * 
     * @return bool
     */
    public function rule()
    {
        return strlen($this->$data) <= $this->params[0];
    }
} 