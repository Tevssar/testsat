<?php
/**
 * Class for Email validation
 *
 * @category Class
 * @package  Satomrutest
 * @author   Bogdan <tevssar379@gmail.com>
 */
namespace Satomrutest\FormValidtor;

/**
 * Class for Email validation
 * 
 * @method rule()
 */
class EmailValidator extends Validator
{

    /**
     * Email validation rule, return false if validation fail.
     * 
     * @return bool
     */
    public function rule()
    {
        $pattern = "/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,})$/i";
        return preg_match($pattern, $this->$data);
    }
}