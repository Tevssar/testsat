<?php
/**
 * User data model.
 *
 * @category Model
 * @package  Satomrutest
 * @author   Bogdan <tevssar379@gmail.com>
 */
namespace FormValidtor\Model;

/**
 * User data model.
 * 
 * @property string $email
 * @property string $name
 * @property date $birthDate
 * 
 * @method __construct($email, $name, $birthDate)
 */
class UserData
{
    public $email;
    public $name;
    public $birthDate;


    /**
     * Fill model.
     * 
     * @param mixed $email     user email
     * @param mixed $name      user name
     * @param mixed $birthDate user birth date
     */
    function __construct($email, $name, $birthDate)
    {
        $this->email = $email;
        $this->name = $name;
        $this->birthDate = $birthDate;
    }

}