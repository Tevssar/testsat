<?php
/**
 * Response model.
 *
 * @category Model
 * @package  Satomrutest
 * @author   Bogdan <tevssar379@gmail.com>
 */
namespace FormValidtor\Model;

/**
 * Response model
 * 
 * @property string $email
 * @property string $title
 * @property string $text
 * 
 * @method __construct($email, $title, $text)
 */
class Response
{
    public $email;
    public $title;
    public $text;

    /**
     * Fill model.
     * 
     * @param string $email user email
     * @param string $title responce title
     * @param string $text  responce body
     */
    function __construct($email, $title, $text)
    {
        $this->email = $email;
        $this->title = $title;
        $this->text = $text;
    }

}