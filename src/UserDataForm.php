<?php
/**
 * Class for edit user data form.
 *
 * @category Class
 * @package  Satomrutest
 * @author   Bogdan <tevssar379@gmail.com>
 */
namespace Satomrutest\FormValidtor;

 use FormValidtor\Model;

/**
 * Class for edit user data form.
 * 
 * @method process()
 * @method renderViev($data)
 */
class ResponseForm extends Form
{

    /**
     * User data processing.
     * 
     * @return object
     */
    public function process()
    {
        $email = new EmailValidator($this->request['email']);
        if (!$email->validate()) {
            $this->errors[] = $email->getError();
        }
        
        $name = new MaxLenghtValidator($this->request['name'], 50);
        if (!$name->validate()) {
            $this->errors[] = $name->getError();
        }

        $birthDate = new DateValidator($this->request['birthDate']);
        if (!$birthDate->validate()) {
            $this->errors[] = $birthDate->getError();
        }

        if (count($this->errors) > 0) {
            $this->renderViev($errors);
            exit();
        }

        return new Model\UserData($email, $name, $birthDate);

    }

    /**
     * Render edit user data form.
     * 
     * @param mixed $data data for viev file.
     */
    public function renderViev($data = [])
    {
        $this->viev('userData', $data);
    }
}


 
