<?php
/**
 * Abstract class for creating various forms.
 *
 * @category Class
 * @package  Satomrutest
 * @author   Bogdan <tevssar379@gmail.com>
 */
namespace Satomrutest\FormValidtor;


/**
 * Abstract class for creating various forms.
 * 
 * @property array $request
 * @property array $error
 * 
 * @method __construct()
 * @method viev($viev, $data)
 * @method process()
 * @method renderViev($data)
 */
abstract class Form
{

    protected $request = [];
    protected $errors = [];

    /**
     * Class constructor. Precess $_REQUEST global variable.
     */
    public function __construct()
    {
        $this->request = $_REQUEST;
    }

    /**
     * Render viev file. 
     * 
     * @param string $viev name of viev
     * @param mixed  $data data for viev file
     */
    protected function viev($viev, $data)
    {
        include_once __DIR__ . "/viev/$viev.php";
    }

    /**
     * Form data processing
     * 
     * @return object
     */
    abstract public function process();

    /**
     * Function for calling viev render.
     * 
     * @param mixed $data data for viev file
     */
    abstract public function renderViev($data = []);

}


