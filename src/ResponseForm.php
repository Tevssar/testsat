<?php
/**
 * Class for response form.
 *
 * @category Class
 * @package  Satomrutest
 * @author   Bogdan <tevssar379@gmail.com>
 */
namespace Satomrutest\FormValidtor;

 use FormValidtor\Model;

/**
 * Class for response form.
 * 
 * @method process()
 * @method renderViev($data)
 */
class ResponseForm extends Form
{

    /**
     * Response data processing.
     * 
     * @return object
     */
    public function process()
    {
        $email = new EmailValidator($this->request['email']);
        if (!$email->validate()) {
            $this->errors[] = $email->getError();
        }
        
        $title = new MaxLenghtValidator($this->request['title'], 100);
        if (!$title->validate()) {
            $this->errors[] = $title->getError();
        }

        $text = new MaxLenghtValidator($this->request['text'], 500);
        if (!$text->validate()) {
            $this->errors[] = $text->getError();
        }

        if (count($this->errors) > 0) {
            $this->renderViev($this->errors);
            exit();
        }

        return new Model\Response($email, $title, $text);

    }

    /**
     * Render response form.
     * 
     * @param mixed $data data for viev file
     */
    public function renderViev($data = [])
    {
        $this->viev('response', $data);
    }
}


