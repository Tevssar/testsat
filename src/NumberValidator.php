<?php
/**
 * Class for check is data are number
 *
 * @category Class
 * @package  Satomrutest
 * @author   Bogdan <tevssar379@gmail.com>
 */
namespace Satomrutest\FormValidtor;

/**
 * Class for check is data are number
 * 
 * @property string $error
 * 
 * @method rule()
 */
class NumberValidator extends Validator
{
    protected $error = 'Number required';

     /**
      * Number check validation rule, return false if validation fail.
      * 
      * @return bool
      */
    public function rule()
    {
        return is_numeric($this->data);
    }
} 
