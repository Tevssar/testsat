<?php
/**
 * Abstract class for creating various validators
 *
 * @category Class
 * @package  Satomrutest
 * @author   Bogdan <tevssar379@gmail.com>
 */
namespace Satomrutest\FormValidtor;


/**
 * Abstract class for creating various validators
 * 
 * @property array $params
 * @property mixed $data
 * @property string $error
 * 
 * @method __construct($data, $params)
 * @method rule()
 * @method validate()
 * @method getError()
 */
abstract class Validator
{
    protected $params;
    protected $data;
    protected $error = 'Something wrong';
 
    /**
     * Class constructor. Get data for validation and params.
     *
     * @param mixed $data   data for validation
     * @param array $params validation params
     */
    public function __construct($data, $params = [])
    {
        $this->params = $params;
        $this->data = $data;
    }

    /**
     * Validation rule, return false if validation fail.
     * 
     * @return bool
     */
    abstract public function rule();

    /**
     * Execute validtion, return false if validation fail.
     * 
     * @return bool
     */
    public function validate()
    {
        $result = $this->rule();
        if ($result) {
            $this->error = false;
        } 
        return $result;
    }
    
    /**
     * Get error message after validation.
     * 
     * @return string
     */
    public function getError() 
    {
        return $this->error;
    }

}


