<?php
/**
 * Class for check is data are date in format of dd/mm/YYYY
 *
 * @category Class
 * @package  Satomrutest
 * @author   Bogdan <tevssar379@gmail.com>
 */
namespace Satomrutest\FormValidtor;

/**
 * Class for check is data are date in format of dd/mm/YYYY
 * 
 * @property string $error
 * 
 * @method rule()
 */
class DateValidator extends Validator
{
    protected $error = 'Date required';

    /**
     * Date dd/mm/YYYY format check validation rule, return false if validation fail.
     * 
     * @return bool
     */
    public function rule()
    {
        $matches = array();
        $pattern = '/^([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$/';
        if (!preg_match($pattern, $this->data, $matches)) {
            return false;
        } 
        if (!checkdate($matches[2], $matches[1], $matches[3])) {
            return false;
        }
        return true;
    }
} 
